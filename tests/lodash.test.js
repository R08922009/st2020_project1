const _ = require('lodash');
const chai = require("chai");
const expect = chai.expect; 


test('1.Should reverse an array', () => {
    const array = [1, 2, 3];
    let reversedArr = _.reverse(array);
    expect(reversedArr).to.eql([3, 2, 1]);
});

test('2.Array Union', () => {
    const array = [1, 2, 3];
    const array2 = [2, 3, 4];
    let unionArr = _.union(array,array2);
    expect(unionArr).to.eql([1, 2, 3, 4]);
});

test('3.Array Intersection', () => {
    const array = [1, 2, 3];
    const array2 = [2, 3, 4];
    let interArr = _.intersection(array,array2);
    expect(interArr).to.eql([2, 3]);
});

test('4.Array Difference', () => {
    const array = [1, 2, 3];
    let difArr = _.difference(array,[1,2]);
    expect(difArr).to.eql([3]);
});

test('5.Delete repeat element in array', () => {
    const array = [1, 2, 2,3,3,4];
    let Arr = _.uniq(array);
    expect(Arr).to.eql([1,2,3,4]);
});

test('6.Get the sum of the elements in array', () => {
    const array = [1, 2, 3,4];
    let Arr = _.sum(array);
    expect(Arr).to.eql(10);
});

test('7.Filled array with character a', () => {
    const array = [1, 2, 3,4];
    let Arr = _.fill(array, 'a');
    expect(Arr).to.eql(['a','a','a','a']);
});

test('8. Should add two numbers', () => {
    expect(_.add(1111, 1222)).to.eql(2333);
});

test('9. Clamp the numbers', () => {
    expect(_.clamp(10 , 0,100)).to.eql(10);
    expect(_.clamp(-10 , 0,100)).to.eql(0);
    expect(_.clamp(1000 , 0,100)).to.eql(100);
});

test('10. Drop some numbers', () => {
    const array = [1, 2, 3,4];
    expect(_.drop(array)).to.eql([2,3,4]);
    expect(_.drop(array,0)).to.eql([1,2,3,4]);
    expect(_.drop(array,3)).to.eql([4]);
});

